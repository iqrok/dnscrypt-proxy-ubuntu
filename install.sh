#!/bin/bash

if [ "${EUID}" != "0" ]; then
	echo "Must be run as root!"
	echo 2
fi

apt purge dnscrypt-proxy

apt update
apt install dnscrypt-proxy resolvconf
APT_STATUS=$?

if [ "${APT_STATUS}" != "0" ]; then
	exit ${APT_STATUS}
fi

NM_DNS_LINE=$(( 2 + $(grep -n "[main]" /etc/NetworkManager/NetworkManager.conf | head -n1 | cut -d: -f1) ))
sed -i "${NM_DNS_LINE} i dns=none" /etc/NetworkManager/NetworkManager.conf

DNSCRYPT_DIR="/etc/dnscrypt-proxy"
cp -f "${DNSCRYPT_DIR}/dnscrypt-proxy.toml" "${DNSCRYPT_DIR}/dnscrypt-proxy.toml.original" 
cp -f "./dnscrypt-proxy.toml" "${DNSCRYPT_DIR}"

#DNS_SERVER_IP=$(dig google.com | grep SERVER | grep -oP "[0-9]{1,3}+\.[0-9]{1,3}+\.[0-9]{1,3}+\.[0-9]{1,3}" | head -n1)

if [ -z "${DNS_SERVER_IP}" ]; then
	echo "DNS SERVER IP is empty : '${DNS_SERVER_IP}'"
	DNS_SERVER_IP="127.0.2.1"
fi

RESOLV_CONF="/etc/resolv.conf"
RESOLV_CONF_ORIDE="${RESOLV_CONF}.override"

echo "nameserver ${DNS_SERVER_IP}" > "${RESOLV_CONF_ORIDE}"

NM_DISPATCHER_DIR="/etc/NetworkManager/dispatcher.d"
NM_DISPATCHER_FILE="${NM_DISPATCHER_DIR}/20-resolv-conf-override"

echo "#!/bin/sh
cp -f ${RESOLVE_CONF_ORIDE} /run/resolvconf/resolv.conf" > "${NM_DISPATCHER_FILE}"
chmod +x "${NM_DISPATCHER_FILE}"
ln -f "${NM_DISPATCHER_FILE}" "${NM_DISPATCHER_DIR}/pre-up.d/"

systemctl restart NetworkManager
systemctl restart dnscrypt-proxy
